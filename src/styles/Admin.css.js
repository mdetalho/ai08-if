// Admin is a very independantly looking part of the app,
// To save time, we can use a separate css file for it.
// Note that it's just a viatant of the main App.css file.

import { StyleSheet } from "react-native";
import { color } from "react-native-reanimated";

export default StyleSheet.create({
  // WELCOME FLOW

  flex_content_box: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
    height: "90%",
    marginTop: "10%",
    },
  
  header_bar: {
    flexDirection: 'row',
    alignItems: 'center',
    // borderBottomWidth: 1,
    borderBottomColor: '#eaeaea',
    padding: 2,
  },

  header_title: {
    fontSize: 25,
    flex: 3,
    alignContent: 'center',
    textAlign: "center",
    bottom: '0%',
    color: '#999999',

    // fontStyle: bold
  },

  body: {
    flexGrow: 10,
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
    alignContent: "center",
  },

  submissions_container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-around",
    alignContent: "center",
    
  },

  sub_image: {
    alignSelf: 'stretch',
    flex: 1,
    width: '100%',
    flexDirection: 'column',
  },

  text_text: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    padding: 5,
    lineHeight: 60,
    backgroundColor: "#00000050",
  },

  challenger_name: {
    flex: 1,
    fontSize: 20,
    fontWeight: "bold",
    color: "#ffffff",
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.8,
    textAlign: "center",
  },

  submission_infos: {
    flex: 1,
    fontSize: 10,
    fontWeight: "bold",
    color: "#dddddd",
    shadowColor: "#000000",
    textShadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.8,
    textAlign: "center",
  },

  submission_card: {
    flex: 2,
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
    alignContent: "center",
    borderWidth: 1,
    borderColor: "#eaeaea",
    borderRadius: 5,
    margin: 5,
    padding: 5,
  },

  challenge_description: {
    marginHorizontal: 20,
    marginVertical: 20,
  },

  challenge_title: {
    marginHorizontal: 20,
    // marginTop: 20,
    textAlign: "center",
    fontSize: 23,
    alignItems: "center",
  },



  bottom_control_bar: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    // float: 'bottom',
  },

  button_nope: {
    padding: 30,
    borderRadius: 500,
    // width: "25%",
    aspectRatio: 1,
    backgroundColor: "#ff9999",
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center",
    
  },

  button_report: {
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center",
        borderRadius: 500,
    width: "20%",
    aspectRatio: 1,
    backgroundColor: "#ffff99",
  },

  button_approve: {

    padding: 30,
    borderRadius: 500,
    // width: "25%",
    aspectRatio: 1,
    backgroundColor: "#99ff99",
    color: "#eaeaea",
  },

  button_icons: {
    fontSize: 40,
    color: "#eaeaea",
    textAlign: "center",
    textAlignVertical: "center",
  },

  button: {
    //display: 'flex',
    position: "relative",
    /* flexDirection: 'row',
    alignItems: 'flex-start', */
    backgroundColor: "rgba(255, 255, 255, 0.7)",
    color: "black",
    borderRadius: 15,
    margin: "5%",
    padding: "5%",
  },
  blur_wrapper: {
    flex: 3,
    display: "flex",
  },
  welcome: {
    fontSize: 20,
    position: "relative",
  },
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-evenly",
    width: "80%",
    left: "10%",
    borderRadius: 15,
    overflow: "hidden",
    paddingBottom: "10%",
    paddingTop: "10%",
  },
  input: {
    color: "#5D1CB0",
    fontSize: 18,
    position: "relative",
    margin: "7%",
    borderBottomColor: "#5D1CB0",
    borderBottomWidth: 2,
    width: "80%",
  },
  confidentiality_title: {
    fontSize: 18,
    fontWeight: "bold",
    width: "60%",
    textAlign: "center",
  },
  permissions: {
    fontSize: 18,
    textAlign: "center",
    margin: "8%",
    textAlign: "center",
  },

  // Profile styles
  my_stats_container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    borderRadius: 15,
    overflow: "hidden",
    paddingTop: "5%",
  },
  my_stats_button: {
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-start",
    backgroundColor: "rgba(255, 255, 255, 0.7)",
    color: "black",
    borderRadius: 15,
    margin: "10%",
    padding: "3%",
  },
  inner_button: {
    fontSize: 12,
    fontWeight: "bold",
    textAlignVertical: "center",
    textAlign: "center",
  },
  my_settings_container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    borderRadius: 15,
    overflow: "hidden",
  },
  my_settings_button: {
    display: "flex",
    zIndex: 1,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "rgba(255, 255, 255, 0.7)",
    color: "black",
    borderRadius: 15,
    margin: "4%",
    padding: "3%",
  },

  purple_button: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    backgroundColor: 'rgba(0, 18, 180, 0.8)',
    borderRadius: 20,
    margin: "1%",
    padding: '1%',
  },

  red_button: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    backgroundColor: 'rgba(255, 66, 66, 1.0)',
    borderRadius: 20,
    margin: "1%",
    padding: '1%',
  },

  green_button: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    backgroundColor: 'rgba(56, 191, 53, 1.0)',
    borderRadius: 20,
    margin: "1%",
    padding: '1%',
  },

  title_purple_button: {
    fontSize: 20, 
    fontWeight: '500', 
    color: 'white',
    padding:'2%'
  },

});
