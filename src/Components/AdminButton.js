import * as React from "react";
import { View, Text, TouchableOpacity, Dimensions, Button } from "react-native";
import { BackgroundImage } from "react-native-elements/dist/config";
// import { BlurView } from 'expo-blur';
import styles from "../styles/Admin.css";

const AdminButton = (props) => {

  return (
    <TouchableOpacity onPress={props.onPress} style={props.style}>
      <Text style={styles.button_icons}>{props.text}</Text>
    </TouchableOpacity>
    // <Text onPress={props.onPress}>AB:{props.text}</Text>
  );
};

export default AdminButton;