import * as React from 'react';
import { Text, View, ImageBackground } from 'react-native';
import { BlurView } from 'expo-blur';
import { TouchableOpacity } from 'react-native-gesture-handler';
import BRLinearGradient from '../Components/Gradients/BRLinearGradient';
import IFLogo from '../Components/IFLogo/IFLogo';
import styles from '../styles/App.css'

const WelcomePage = ({ navigation }) => {

  const navigateToNextPage = () => {
      navigation.replace('Authentification');
  };

  const if_background = require('../../assets/if_background.jpg')


  return (
    <ImageBackground
        source={if_background}
        style={{width: '100%', height: '100%'}}
        resizeMode= 'cover'
      >
      <BRLinearGradient/>
        <View style={{flex:1, justifyContent: 'space-evenly'}}>
          <IFLogo/>
          <View style={styles.blur_wrapper}>
            <BlurView intensity={80} tint="light" style={styles.container}>
              <Text style={styles.welcome, {fontSize: 30, fontWeight: 'bold'}}>Bienvenue</Text>
              <Text style={styles.welcome}>Ils clairieres jeu nationales infiniment. Relatif uns circule peuples comprit nez galoper immense peu. Condamnait non fut frissonner eue c'est super l'IF j'ai trop hâte d'y être</Text>
              <TouchableOpacity style={styles.button} onPress= {navigateToNextPage} >
                <Text style={styles.welcome_title_button}>C'est parti ! </Text>
              </TouchableOpacity>
            </BlurView>
          </View>
        </View>   
    </ImageBackground>
  );
};
export default WelcomePage;