import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

const BackIcon = ({ onPress }) => {
  const insets = useSafeAreaInsets();

  return (
    <TouchableOpacity
      style={{
        top: insets.top,
        left: insets.left,
        marginTop: 5,
        marginLeft: 5,
        zIndex: 1
      }}
      onPress={onPress}
    >
      <Ionicons name= "arrow-back" size={32} color="black" />
    </TouchableOpacity>
  );
};

export default BackIcon;
