import * as React from "react";
import {
  View,
  Text,
  ImageBackground,
} from "react-native";
import styles from "../../../styles/Admin.css";
import { Avatar } from "react-native-elements";

const SubCard = (props) => {

  const users = [
    {
      name: "brynn",
      avatar: "https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg",
    },
  ];
  return (
    <View style={styles.submission_card}>

      <ImageBackground
        source={props.submission.image}
        resizeMode="cover"
        style={styles.sub_image}
      >
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          padding: 20,
          justifyContent: "space-between",
        }}
      >
        <Avatar
          rounded
          title="M"
          size="medium"
          overlayContainerStyle={{ backgroundColor: "green" }}
        />
        <Text style={styles.challenger_name}>
          {props.submission.challenger}
        </Text>
        <View>
          <Text style={styles.submission_infos}>
            Submission ID : {props.submission.key}
          </Text>
          <Text style={styles.submission_infos}>
            Challenge ID : {props.submission.challenge_id}
          </Text>
        </View>
      </View>
        
      </ImageBackground>
    </View>
  );
};

const ListSubmissionCards = (props) => {
  // J'ai utilisé une class conposant dans l'espoir d'arriver à mettre à jour les cartes vsibles ou non.

    submissions = props.submmissions;
    state = { count: 1 };
    cards = props.submissions.map((sub) => (
      <SubCard submission={sub} key={sub.key} />
    ));

    return (
      <View style={styles.submissions_container}>
        <>{cards[props.active_card]}</>
      </View>
    );

};


export default ListSubmissionCards;
