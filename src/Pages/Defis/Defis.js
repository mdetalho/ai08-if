import * as React from 'react';
import { View, Text, Image, TouchableOpacity, Dimensions, ImageBackground } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import ScrollGradient from '../../Components/Gradients/ScrollGradient';
import MyDefis from '../../Components/Users/Defis/MyDefis';
import styles from '../../styles/App.css';

const Defis = ({navigation}) => {

  const if_background = require('../../../assets/if_background.jpg')
  const defis = require('../../../assets/defis.png')
  const imageWidth = Dimensions.get('window').width / 10
  const goToAdmin = () => {
    navigation.navigate({name:'InfoDefis', title:'Faire un selflie avec Joris'});
  };
  return(
    <ImageBackground
      source={if_background}
      style={{width: '100%', height: '100%'}}
      resizeMode= 'cover'
    >
      <ScrollGradient/>
      <ScrollView style={{flex: 1}} contentContainerStyle={{ flexGrow: 1}} contentContainerStyle={{alignItems: 'center'}}>
        <View style={{flex:1, top: 50, width: '90%'}}>         
          <View style= {styles.pages_title_container}>
            <Image 
            source={defis}
            resizeMode='cover'
            style={{
              width: imageWidth,
              height: imageWidth,
              margin:'2%'
            }}
            />
            <Text style={{
              fontSize: 25, 
              color: 'rgba(0, 18, 180, 1.0)',
              fontWeight: 'bold'
            }}> 5 défis disponibles </Text>
          </View>
        </View>
        <View style= {{width: '100%', height:'100%', alignItems:'center', marginBottom: '56%', marginTop: 50}}>
          <MyDefis title = 'Faire un selflie avec Joris' numb = {3} place = 'Stand de crêpe' time = '2h' points = '30' etat = {0} description = 'Ce défis consiste à trouver Joris dans le Festival ! Si vous le trouvez, faites un selfie avec lui ! Joris le Gecko dans le festival pour pouvoir prendre une photo avec lui et de l envoyer. Il peut être partout car il peut marcher sur les murs et les plafonds ! La chance !!' />
          <MyDefis title = 'Faire un selflie avec Tom' numb = {2} place = 'C212' time = '2h' points = '30' etat = {1} description = 'Ce défis consiste à trouver Tom dans le Festival ! Si vous le trouvez, faites un selfie avec lui ! Joris le Gecko dans le festival pour pouvoir prendre une photo avec lui et de l envoyer. Il peut être partout car il peut marcher sur les murs et les plafonds ! La chance !!'/>
          <MyDefis title = 'Faire un selflie avec Tom' numb = {2} place = 'C212' time = '2h' points = '30' etat = {2} description = 'Ce défis consiste à trouver Joris dans le Festival ! Si vous le trouvez, faites un selfie avec lui ! Joris le Gecko dans le festival pour pouvoir prendre une photo avec lui et de l envoyer. Il peut être partout car il peut marcher sur les murs et les plafonds ! La chance !!'/>
          <MyDefis title = 'Faire un selflie avec Tom' numb = {2} place = 'C212' time = '2h' points = '30' etat = {3} description = 'Ce défis consiste à trouver Joris dans le Festival ! Si vous le trouvez, faites un selfie avec lui ! Joris le Gecko dans le festival pour pouvoir prendre une photo avec lui et de l envoyer. Il peut être partout car il peut marcher sur les murs et les plafonds ! La chance !!' />
          <MyDefis title = 'Faire un selflie avec Tom' numb = {2} place = 'C212' time = '2h' points = '30' etat = {4} description = 'Ce défis consiste à trouver Joris dans le Festival ! Si vous le trouvez, faites un selfie avec lui ! Joris le Gecko dans le festival pour pouvoir prendre une photo avec lui et de l envoyer. Il peut être partout car il peut marcher sur les murs et les plafonds ! La chance !!'/>
          <MyDefis title = 'Faire un selflie avec Tom' numb = {2} place = 'C212' time = '2h' points = '30' etat = {5} description = 'Ce défis consiste à trouver Joris dans le Festival ! Si vous le trouvez, faites un selfie avec lui ! Joris le Gecko dans le festival pour pouvoir prendre une photo avec lui et de l envoyer. Il peut être partout car il peut marcher sur les murs et les plafonds ! La chance !!'/>
          <MyDefis title = 'Faire un selflie avec Tom' numb = {2} place = 'C212' time = '2h' points = '30' etat = {6} description = 'Ce défis consiste à trouver Joris dans le Festival ! Si vous le trouvez, faites un selfie avec lui ! Joris le Gecko dans le festival pour pouvoir prendre une photo avec lui et de l envoyer. Il peut être partout car il peut marcher sur les murs et les plafonds ! La chance !!'/>
          <MyDefis title = 'Faire un selflie avec Tom' numb = {2} place = 'C212' time = '2h' points = '30' etat = {6} description = 'Ce défis consiste à trouver Joris dans le Festival ! Si vous le trouvez, faites un selfie avec lui ! Joris le Gecko dans le festival pour pouvoir prendre une photo avec lui et de l envoyer. Il peut être partout car il peut marcher sur les murs et les plafonds ! La chance !!'/>
          <MyDefis title = 'Faire un selflie avec Tom' numb = {2} place = 'C212' time = '2h' points = '30' etat = {6} description = 'Ce défis consiste à trouver Joris dans le Festival ! Si vous le trouvez, faites un selfie avec lui ! Joris le Gecko dans le festival pour pouvoir prendre une photo avec lui et de l envoyer. Il peut être partout car il peut marcher sur les murs et les plafonds ! La chance !!'/>
          <MyDefis title = 'Faire un selflie avec Tom' numb = {2} place = 'C212' time = '2h' points = '30' etat = {6} description = 'Ce défis consiste à trouver Joris dans le Festival ! Si vous le trouvez, faites un selfie avec lui ! Joris le Gecko dans le festival pour pouvoir prendre une photo avec lui et de l envoyer. Il peut être partout car il peut marcher sur les murs et les plafonds ! La chance !!'/>
          <MyDefis title = 'Faire un selflie avec Tom' numb = {2} place = 'C212' time = '2h' points = '30' etat = {6} description = 'Ce défis consiste à trouver Joris dans le Festival ! Si vous le trouvez, faites un selfie avec lui ! Joris le Gecko dans le festival pour pouvoir prendre une photo avec lui et de l envoyer. Il peut être partout car il peut marcher sur les murs et les plafonds ! La chance !!'/>
          <MyDefis title = 'Faire un selflie avec Tom' numb = {2} place = 'C212' time = '2h' points = '30' etat = {6} description = 'Ce défis consiste à trouver Joris dans le Festival ! Si vous le trouvez, faites un selfie avec lui ! Joris le Gecko dans le festival pour pouvoir prendre une photo avec lui et de l envoyer. Il peut être partout car il peut marcher sur les murs et les plafonds ! La chance !!'/>
          <MyDefis title = 'Faire un selflie avec Tom' numb = {2} place = 'C212' time = '2h' points = '30' etat = {6} description = 'Ce défis consiste à trouver Joris dans le Festival ! Si vous le trouvez, faites un selfie avec lui ! Joris le Gecko dans le festival pour pouvoir prendre une photo avec lui et de l envoyer. Il peut être partout car il peut marcher sur les murs et les plafonds ! La chance !!'/>
        </View>
      </ScrollView>

    </ImageBackground>
  )
}

export default Defis
