import * as React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { BlurView } from 'expo-blur';
import styles from '../../../styles/App.css';
import Modal from 'react-native-modal';
import FlipCard from 'react-native-flip-card'


const MyRewards = () => {

  const [isModalVisible, setIsModalVisible] = React.useState(false);
  const handleModal = () => setIsModalVisible(() => !isModalVisible);

    return( 
      <BlurView intensity={80} tint="light" style={styles.my_rewards_container}>
        <View style={{flexDirection:'row'}}>
          <Text style={styles.title}>Accéder aux loges de Vald</Text>
        </View>
        <View style={{flexDirection:'row', justifyContent:'center'}}>
          <FlipCard>
            {/* Face Side */}
            <View style={styles.face}>
              <Text style={styles.title_purple_button}>Récupérer</Text>
            </View>
            {/* Back Side */}
            <View style={styles.face}>
              <Text style={styles.title_purple_button}>ABC-123</Text>
            </View>
          </FlipCard>
          <TouchableOpacity style={styles.white_button} onPress= {() => handleModal()} >
            <Text style={styles.title_button}>En savoir plus</Text>
              <Modal isVisible={isModalVisible}>
                <View style={styles.modal_container}>
                  <Text style={styles.title}>Accéder aux loges de Vald</Text>
                  <Text style={styles.body}>Ils clairieres jeu nationales infiniment. Relatif uns circule peuples comprit nez galoper immense peu. Condamnait etonnement non fut frissonner eue. Pour récupérer ta récompense, cliquer sur Récupérer et présente le code à l'accueuil de l'IF.</Text>
                  <TouchableOpacity style={styles.purple_button} onPress={handleModal}>
                    <Text style={styles.title_purple_button}>Fermer</Text>
                  </TouchableOpacity>
                </View>
              </Modal>
          </TouchableOpacity>
        </View>
        
      </BlurView>
    )
}

export default MyRewards