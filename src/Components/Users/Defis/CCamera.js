import React from 'react';
import { View } from 'react-native';
import { Camera } from 'expo-camera';
import BackIcon from '../../BackIcon';

const CCamera = ({navigation}) => (
  <View style={{flex: 10, justifyContent: 'center',}}>
    <BackIcon onPress= {() => navigation.goBack('Defis')}/>
    <Camera style={{flex: 1, justifyContent: 'flex-end', alignItems: 'center',}}>
      <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }} />
    </Camera>
  </View>
);

export default CCamera;
