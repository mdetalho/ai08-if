import * as React from 'react';
import { Text, TouchableOpacity } from 'react-native';


/* Le props.etat permet de savoir dans quel état le défis doit être voici la table de correspondance :
    - 0 : Le défis est disponible, on peut y participer
    - 1 : Le défis est disponible, on y a déjà fait une ou plusieurs tentatives ratées
    - 2 : Le défis à été envoyé aux admins pour une validation, on est en attente du résultat
    - 3 : Le défis à été validé !
    - 4 : Le défis a été raté ! On a épuisé toutes les tentatives..
    - 5 : Le défis est bientôt disponible
    - 6 : Le défis est terminé car on on a dépassé sa limite de temps
*/
  
const MyButton = (props) => {
    var colorInsideButton = '#FFFFFF'
    var colorButton = 'rgba(0, 18, 180, 0.8)'
    var textInsideButton = 'Participer'
    var buttonDisabled = false

    if (props.etat == 1) {
      textInsideButton = 'Réessayer'
    }
    else if (props.etat == 2) {
      colorInsideButton = '#4C4C4C'
      colorButton = '#D1D1D1'
      textInsideButton = 'En attente de validation'
      buttonDisabled = true
    }
    else if (props.etat == 3) {
      colorInsideButton = '#00B11C'
      colorButton = '#B8FFC7'
      textInsideButton = 'Validé ! :)'
      buttonDisabled = true
    }
    else if (props.etat == 4) {
      colorInsideButton = '#B10000'
      colorButton = '#FFB8B8'
      textInsideButton = 'Raté.. :('
      buttonDisabled = true
    }
    else if (props.etat == 5) {
      colorInsideButton = '#4C4C4C'
      colorButton = '#D1D1D1'
      textInsideButton = 'Bientôt disponible'
      buttonDisabled = true
    }
    else if (props.etat == 6) {
      colorInsideButton = '#4C4C4C'
      colorButton = '#D1D1D1'
      textInsideButton = 'Terminé'
      buttonDisabled = true
    }
    return (
        <TouchableOpacity 
        disabled = {buttonDisabled}
        onPress = {props.onPress}
        style={{
            width : '85%',
            backgroundColor: `${colorButton}`,
            color: 'black',
            borderRadius: 20,
            paddingVertical: '2%',
            paddingHorizontal: '2%',
        }}>
        <Text style={{
            fontSize: 18,
            fontWeight: 'bold',
            textAlign: "center",
            color : `${colorInsideButton}`,
            }}>
            {textInsideButton}
        </Text>
        </TouchableOpacity>
    )
} 

export default MyButton;
