import React from 'react';
import { View,Text, TouchableOpacity, StyleSheet } from 'react-native';
import { BlurView } from 'expo-blur';
import styles from '../../../styles/App.css'
import { useNavigation } from '@react-navigation/native'

const TopUsers = () => {

  const navigation = useNavigation()

  const goToTopUsers = () => {
    navigation.navigate('TopFiftyUsers');
  };

  const top_users = [
    { name: "Mayo", position: "1", points: "2000" },
    { name: "Lou", position: "2", points: "1500" },
    { name: "Pierre", position: "3", points: "1000" },
    { name: "Vous", position: "15", points: "800" },
    { name: "Joris", position: "16", points: "700" },
  ]

    return( 
      <View style={{top: '15%'}}>
      <View style={styles.title_container}>
        <Text style={styles.purple_title}> Classement général </Text>
      </View>
      <BlurView intensity={80} tint="light" style={styles.my_stats_container}>
        <Text style={{fontSize:17, fontWeight: '600'}}> 250 festivaliers inscrits </Text>
        <View style={{flex: 1, flexDirection:'row', justifyContent: 'space-around', left: '2%', top: '3%'}}>
          <View style={{flex: 1, flexDirection: 'column', alignItems:'stretch'}}>
            {top_users.map((top_user, index) => {
              const text_style = top_user.name === "Vous" ? styles.current_user : styles.other_user
              return(
                <View key={index} style={{flex: 1, flexDirection: 'row', justifyContent: 'space-evenly'}} >
                  <Text style={{...text_style, width: '10%'}}>{top_user.position}</Text>
                  <Text style={{...text_style, width: '25%'}}>{top_user.name}</Text>
                  <Text style={{...text_style, width: '25%'}}> {top_user.points}</Text>
                  <Text style={{...text_style, width: '10%'}}>pts</Text>
                </View>
              )
            })}
          </View>
        </View>
        <View style={{marginTop: 5}}>
          <TouchableOpacity style={styles.white_button} onPress={() => goToTopUsers()}>
            <Text style={styles.title_button}>Voir classement général</Text>
          </TouchableOpacity>
        </View>
      </BlurView>
      </View>
    )
};
export default TopUsers