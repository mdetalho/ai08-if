import React, { useState } from 'react';
import { View,Text, TouchableOpacity } from 'react-native';
import { BlurView } from 'expo-blur';
import { ScrollView } from 'react-native-gesture-handler';
import ScrollGradient from '../../Gradients/ScrollGradient';
import styles from '../../../styles/App.css'
import BackIcon from '../../BackIcon';

const Top50 = ({navigation}) => {

  const goToProfil = () => {
    navigation.goBack();
  };

  const top_users = [
    { name: "Mayo", position: "1", points: "2000" },
    { name: "Lou", position: "2", points: "1500" },
    { name: "Pierre", position: "3", points: "1000" },
    { name: "Jacques", position: "4", points: "999" },
    { name: "Pauline", position: "5", points: "990" },
    { name: "Marie", position: "6", points: "989" },
    { name: "Jules", position: "7", points: "970" },
    { name: "Marc", position: "8", points: "969" },
    { name: "Vous", position: "15", points: "800" },
    { name: "Joris", position: "16", points: "700" },
  ]

  return( 
    <>
    <ScrollGradient/>
      <BackIcon onPress= {() => goToProfil()}/>
      <ScrollView contentContainerStyle={{ flexGrow: 1, alignItems: 'center'}}>
        <View style={{top: '10%', alignItems: 'center', width: '80%'}}>
        <View style={styles.title_container}>
          <Text style={styles.purple_title}> Classement général </Text>
        </View>
        <BlurView intensity={80} tint="light" style={styles.my_stats_container}>
          <Text style={{fontSize:17, fontWeight: '600'}}> 250 festivaliers inscrits </Text>
            <View style={{flexDirection:'row',  justifyContent: 'space-around', paddingTop: '5%', paddingBottom: '5%'}}>
              <View style={{flex: 1, flexDirection: 'column', alignItems: 'center'}}>
                {top_users.map((top_user, index) => {
                  const text_style = top_user.name === "Vous" ? styles.current_user : styles.other_user
                    return(
                      <View key={index} style={{flex: 1, flexDirection: 'row', justifyContent: 'space-evenly'}} >
                        <Text style={{...text_style, width: '10%'}}>{top_user.position}</Text>
                        <Text style={{...text_style, width: '25%'}}>{top_user.name}</Text>
                        <Text style={{...text_style, width: '25%'}}> {top_user.points}</Text>
                        <Text style={{...text_style, width: '10%'}}>pts</Text>
                      </View>
                    )
                })}
              </View>
            </View>
        </BlurView>
        </View>
      </ScrollView>
    </>
  )
}

export default Top50