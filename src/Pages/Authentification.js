import * as React from 'react';
import {
  Text, View, ImageBackground, StyleSheet, TextInput
} from 'react-native';
import { BlurView } from 'expo-blur';
import { TouchableOpacity } from 'react-native-gesture-handler';
import styles from '../styles/App.css'

import BRLinearGradient from '../Components/Gradients/BRLinearGradient';
import IFLogo from '../Components/IFLogo/IFLogo';


const Authentification = ({ navigation }) => {

  const goToNextPage = () => {
    navigation.navigate('Permissions')
  }

  const if_background = require('../../assets/if_background.jpg')
  const [email, onChangeEmail] = React.useState();
  const [password, onChangePassword] = React.useState("");
  const [number, onChangeNumber] = React.useState("");

  return (
    <ImageBackground
      source={if_background}
      style={{width: '100%', height: '100%'}}
      resizeMode= 'cover'
    >
    <BRLinearGradient/>
      <View style={{flex:1, justifyContent: 'space-evenly'}}>
        <IFLogo/>
        <View style={styles.blur_wrapper}>
          <BlurView intensity={80} tint="light" style={styles.container}>
            <TextInput style={styles.input} onChangeText={onChangeEmail} placeholder={"Email"} value={email}/>
            <TextInput style={styles.input} onChangeText={onChangePassword} autoComplete="password"  secureTextEntry={true} placeholder={"Password"} value={password}/>
            <TextInput style={styles.input} onChangeText={onChangeNumber} placeholder={"Ticket Number"} value={number}/>
            <TouchableOpacity style={styles.button} onPress= {goToNextPage} >
              <Text style={styles.welcome_title_button}> Se connecter </Text>
            </TouchableOpacity>
          </BlurView>
        </View>
      </View>   
    </ImageBackground>
    );
}
export default Authentification