import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Authentification from './src/Pages/Authentification';
import PermissionsPage from './src/Pages/Permissions';
import WelcomePage from './src/Pages/WelcomePage';
import Main from './src/Pages/Main';
import Admin from './src/Pages/Admin'
import Defis from './src/Pages/Defis/Defis'
import InfoDefis from './src/Pages/Defis/InfoDefis'
import Top50 from './src/Components/Users/Stats/Top50';
import CCamera from './src/Components/Users/Defis/CCamera';

const Stack = createStackNavigator();

const App = () =>  {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="WelcomePage" screenOptions={{ headerShown: false }}>
        <Stack.Screen name="WelcomePage" component={WelcomePage} />
        <Stack.Screen name="Authentification" component={Authentification} />
        <Stack.Screen name="Permissions" component={PermissionsPage} />
        <Stack.Screen name="Main" component={Main} />
        <Stack.Screen name="Admin" component={Admin} />
        <Stack.Screen name="Defis" component={Defis} />
        <Stack.Screen name="InfoDefis" component={InfoDefis} />
        <Stack.Screen name="TopFiftyUsers" component={Top50} />
        <Stack.Screen name="Camera" component={CCamera} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;