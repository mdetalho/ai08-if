import * as React from 'react';
import { View, Text, Image, Dimensions, ImageBackground } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import ScrollGradient from '../Components/Gradients/ScrollGradient';
import MySettings from '../Components/MySettings';
import MyStats from '../Components/Users/Stats/MyStats';
import TopUsers from '../Components/Users/Stats/TopUsers';

const Profile = ({navigation}) => {

  const profile = require('../../assets/profile.jpg')
  const if_background = require('../../assets/if_background.jpg')
  const imageWidth = Dimensions.get('window').width / 3
  const imageHeight = Dimensions.get('window').height / 7

  return(
    <ImageBackground
      source={if_background}
      style={{width: '100%', height: '100%'}}
      resizeMode= 'cover'
    >
    <ScrollGradient/>
      <ScrollView style={{flex: 1}} contentContainerStyle={{ flexGrow: 1}} contentContainerStyle={{alignItems: 'center'}}>
        <View style={{flex:1, top: 50, width: '90%'}}>
          <View style= {{alignItems:'center', justifyContent:'center'}}>
            <Image 
              source={profile}
              resizeMode='cover'
              style={{
                width: imageWidth,
                height: imageHeight,
                borderRadius: 60,
                borderWidth: 1,
                borderColor: 'rgba(180, 0, 162, 1.0)',
                margin:'5%'
              }}
              />
            <Text style={{
              fontSize: 25, 
              color: 'rgba(255, 255, 255, 1.0)',
              fontWeight: 'bold'
            }}> Emmanuel Macron </Text>
          </View>
        </View>

        <View style={{width: '90%'}}>
          <View style= {{alignItems:'center', justifyContent:'center'}}></View>
          <MyStats navigation = {navigation}/>
          <TopUsers/>
          <MySettings navigation = {navigation}/>
        </View>
        <View style={{height:300}}></View>
      </ScrollView>
    </ImageBackground>
  )
}

export default Profile