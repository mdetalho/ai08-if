import * as React from 'react';
import { View, Text, Dimensions, Image, ImageBackground} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import ScrollGradient from '../Components/Gradients/ScrollGradient';
import Post from '../Components/Users/Feed/Post';
import styles from '../styles/App.css'

const FeedImaginarium = ({ navigation }) => {

  const award = require('../../assets/award.png')
  const imageWidth = Dimensions.get('window').width / 10
  const if_background = require('../../assets/if_background.jpg')

  return(
    <ImageBackground
      source={if_background}
      style={{width: '100%', height: '100%'}}
      resizeMode= 'cover'
    >
    <ScrollGradient/>
      <ScrollView style={{flex: 1}} contentContainerStyle={{ flexGrow: 1}} contentContainerStyle={{alignItems: 'center'}}>
      
        <View style={{flex:1, top: 50, width: '90%'}}>         
          <View style= {styles.pages_title_container}>
              <Image 
              source={award}
              resizeMode='cover'
              style={{
                width: imageWidth,
                height: imageWidth,
                margin:'2%'
              }}
              />
            <Text style={{
              fontSize: 25, 
              color: 'rgba(0, 18, 180, 1.0)',
              fontWeight: 'bold'
            }}> Fil d'actualité </Text>
          </View>
        </View>
        <View style={{marginTop: 50, width: '90%'}}>
          <View style= {{alignItems:'center', justifyContent:'center'}}></View>
          <Post/>
          <Post/>
          <Post/>
        </View>
        <View style={{height:200}}></View>
      </ScrollView>
    </ImageBackground>
  )
}

export default FeedImaginarium