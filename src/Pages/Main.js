import * as React from 'react';
import { Image, View, Text, StyleSheet } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Defis from './Defis/Defis';
import FeedImaginarium from './FeedImaginarium';
import Profile from './Profile';
import Rewards from './Rewards';


const Tab = createBottomTabNavigator();

const Main = () => {
  const profile = require('../../assets/profile.jpg')
  
  return (
    <Tab.Navigator
      screenOptions= {{
        scrollEnabled: true,
        tabBarShowLabel: false,
        tabBarStyle: {
          position: 'absolute',
          bottom: 25,
          left: '5%',
          right: '5%',
          elevation: 0,
          backgroundColor: 'white',
          borderRadius: 15,
          height: 60,
          ...styles.shadow
        },
        tabBarItemStyle: {
          flex: 1,
          alignItems:'center', 
          justifyContent:'center',
          position:'relative',
          height: 60,
          width: '100%'
        },
        headerShown: false,
      }}
      >
      <Tab.Screen name = "Profile" component={Profile} options={{
        tabBarIcon: ({focused}) => (
          <View>
            <Image 
              source={profile}
              resizeMode='cover'
              style={{
                width: 50,
                height: 50,
                borderRadius: 40,
                borderWidth: 1,
                borderColor: focused? 'rgba(180, 0, 162, 0.5)': '#748c94'
              }}
              />
          </View>
        )
        }}/>
      <Tab.Screen name = "Defis" component={Defis} style={{width: '40%' }} options={{
        tabBarIcon: ({focused}) => (
          <View>
            <Text style={{
                color: focused?  'rgba(180, 0, 162, 0.5)': '#748c94',
                fontSize: 15,
              }}>Defis</Text>
          </View>
        )
      }}/>
      <Tab.Screen name = "IfFeed" component={FeedImaginarium} options={{
        tabBarIcon: ({focused}) => (
          <View>
            <Text style={{
                color: focused?  'rgba(180, 0, 162, 0.5)': '#748c94',
                fontSize: 15,
              }}>IfFeed</Text>
          </View>
        )
      }}/>
      <Tab.Screen name = "Rewards" component={Rewards} options={{
        tabBarIcon: ({focused}) => (
          <View style={{alignItems:'center', justifyContent:'center'}}>
            <Text style={{
                color: focused? 'rgba(180, 0, 162, 0.5)': '#748c94',
                fontSize: 15
              }}>Prix</Text>
          </View>
        )
      }}/>
    </Tab.Navigator>

  );
};

const styles = StyleSheet.create({
  shadow: {
    shadowColor: 'black',
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.4,
    shadowRadius: 3.5,
    elevation: 5,
  },
})

export default Main;