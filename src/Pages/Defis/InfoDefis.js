import * as React from 'react';
import { View, Text, Image, Dimensions, TouchableOpacity} from 'react-native';
import { BlurView } from 'expo-blur';
import { ScrollView } from 'react-native-gesture-handler';
import ScrollGradient from '../../Components/Gradients/ScrollGradient';
import styles from '../../styles/App.css';
import BackIcon from '../../Components/BackIcon';
import MyButton from '../../Components/MyButton';

const InfoDefis = ({route, navigation}) => {
    const title = route.params.title
    const numb = route.params.numb
    const place = route.params.place
    const time = route.params.time
    const points = route.params.points
    const description = route.params.description
    const etat = route.params.etat
    const position = require('../../../assets/icon_position.png')
    const map_position = require('../../../assets/position_map.png')
    
    const goToDefis = () => {
        navigation.goBack('Defis');
    };
    return(
    <>
      <ScrollGradient/>
      <BackIcon onPress= {() => goToDefis()}/>
      <ScrollView style={{flex: 1}} contentContainerStyle={{ flexGrow: 1}} contentContainerStyle={{alignItems: 'center'}}>
          <View style={{flex:1, marginTop: '15%', width: '90%', height: '100%'}}>
            <Text 
                style={{
                    fontWeight: 'bold',
                    fontSize: 30,
                    color : "rgba(255,255,255, 0.8)"
                }}>
                {title}
            </Text>
            <BlurView 
                intensity={80}
                tint="light"
                style={{
                    display:'flex',
                    alignItems:'center',
                    borderRadius: 25, 
                    overflow: 'hidden',
                    paddingVertical : '7%',
                    paddingHorizontal : '2%',
                    marginTop: "3%"
                }}>
                <MyButton etat = {etat} />
                <BlurView 
                    intensity={80}
                    tint="light"
                    style={{
                        borderRadius: 25, 
                        overflow: 'hidden',
                        padding : '2%',
                        display : 'flex',
                        flexDirection: 'row',
                        marginHorizontal : '1%',
                        marginVertical: "3%"
                    }}>
                    <Text style={{fontSize: 17, fontWeight: 'bold', marginHorizontal: '2%'}}>
                        {numb}
                    </Text>
                    <Text style={{fontSize: 17}}>
                        tentatives restantes
                    </Text>
                </BlurView>
                <View style={{width:'100%', display: 'flex', flexDirection: 'row', justifyContent:'center'}}>
                    <BlurView intensity={80} tint="light" style={styles.my_defis_settings_blur}>
                        <Text style={{fontSize: 17}}>
                        {time} restantes
                        </Text>
                    </BlurView>
                    <BlurView intensity={80} tint="light" style={styles.my_defis_settings_blur}>
                        <Text style={{fontSize: 17}}>
                            + {points} pts
                        </Text>
                    </BlurView>
                </View>
                <View style={{width:'90%', display: 'flex', flexDirection: 'row', justifyContent:'flex-start', padding:'4%'}}>
                    <Text style={{fontSize: 17, textAlign:'justify'}}>
                        {description}
                    </Text>
                </View>
                <View style={{width:'90%', height:70, display: 'flex', flexDirection: 'row', justifyContent:'flex-start', alignItems:'center', paddingVertical:'3%', paddingHorizontal:'4%'}}>
                    <Image 
                        source={position}
                        resizeMode='contain'
                        style={{
                            width: '10%',
                            height: '90%',
                            marginRight: '5%'
                        }}
                    />
                    <Text style={{fontSize: 17, textAlign:'justify'}}>
                        {place} 
                    </Text>
                </View>
                <View style={{width:'90%', display: 'flex', flexDirection: 'row', justifyContent:'center'}}>
                    <Image 
                        source={map_position}
                        resizeMode='cover'
                        style={{
                            borderRadius: 30,
                            width: '100%',
                            height: 250
                        }}
                    />
                </View>
            </BlurView>
          </View>
          <View style={{height:50}}></View>
      </ScrollView>
    </>
  )
}

export default InfoDefis
