import * as React from 'react';
import { View, Text,Image, Dimensions } from 'react-native';
import { BlurView } from 'expo-blur';
import styles from '../../../styles/App.css'
import { useNavigation, Link } from '@react-navigation/native'
import MyButton from '../../MyButton';


  const MyDefis = (props) => {
    const title = props.title
    const numb = props.numb
    const place = props.place
    const time = props.time
    const points = props.points
    const description = props.description
    const etat = props.etat
    const navigation = useNavigation()
    const goToInfoDefis = () => {
      navigation.navigate('InfoDefis', {title: title, numb: numb, place: place, time: time, points: points, etat: etat, description: description});
    };
    const goToCamera = () => {
      navigation.navigate('Camera')
    }

    const info = require('../../../../assets/infos.png')
    const Width = Dimensions.get('window').width

    return (
      <View style={{width: Width,  padding: '4%'}}>
        <Link to={{ screen: 'InfoDefis', params: {title: title, numb: numb, place: place, time: time, points: points, etat: etat, description: description}}}>
        <BlurView style={styles.my_defis_settings_container}
          intensity={80}
          tint="light">
          <Text style={{margin: '4%', fontSize: 20, fontWeight: 'bold'}}>
            {props.title}
          </Text>
          <View
            style={{
              width:'100%',
              display: 'flex',
              flexDirection: 'row',
              flexWrap: 'wrap',
              alignItems : 'center',
              justifyContent: 'flex-end',
              margin : '%',
              }}>
              <BlurView intensity={80} tint="light"
                style={styles.my_defis_settings_blur}>
                <Text style={{fontSize: 17}}>
                  {props.place}
                </Text>
              </BlurView>
              <BlurView intensity={80} tint="light"
                style={styles.my_defis_settings_blur}>
                <Text style={{fontSize: 17}}>
                  {props.time} restantes
                </Text>
              </BlurView>
              <BlurView intensity={80} tint="light"
                style={styles.my_defis_settings_blur}>
                <Text style={{fontSize: 17}}>
                  + {props.points} pts
                </Text>
              </BlurView>
          </View>
          <View
            style={{
              width:'100%',
              display: 'flex',
              alignItems: 'flex-end',
              margin : '1%',
              }}>
              <BlurView 
                intensity={80}
                tint="light"
                style={{
                  borderRadius: 25, 
                  overflow: 'hidden',
                  padding : '2%',
                  display : 'flex',
                  flexDirection: 'row',
                  marginHorizontal : '1%'
                  }}>
                <Text style={{fontSize: 17, fontWeight: 'bold', marginHorizontal: '2%'}}>
                  {props.numb}
                </Text>
                <Text style={{fontSize: 17}}>
                   tentatives restantes
                </Text>
              </BlurView>
          </View>
          <View style={{width: '100%', display:'flex', flexDirection: 'row', alignItems: 'center'}}>
            <MyButton etat = {props.etat} onPress= {() => goToCamera()}/>
            <Image 
              source={info}
              resizeMode='contain'
              style={{
                width: '10%',
                height: '100%',
                marginHorizontal: '3%',
              }}
              />
          </View>
        </BlurView>
        </Link>
      </View> 
    );
  };
  export default MyDefis;