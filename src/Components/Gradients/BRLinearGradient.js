import * as React from 'react';
import { LinearGradient } from 'expo-linear-gradient';

const BRLinearGradient = () => {

    return (
        <>
        <LinearGradient
        colors={['rgba(180, 0, 162, 0.5)', 'transparent']}
        start={{x: 1, y:  1}} 
        end={{x: 0, y: 0}}
        style={{position: 'absolute',  top: 0, bottom: 0, left: 0, right: 0, backgroundColor: "transparent"}}
        />
        <LinearGradient
        colors={['rgba(0, 18, 180, 0.5)', 'transparent']}
        start={{x: 0, y:  0}} 
        end={{x: 1, y: 1}}
        style={{position: 'absolute',  top: 0, bottom: 0, left: 0, right: 0, backgroundColor: "transparent"}}
        />
        </>
    )
}

export default BRLinearGradient