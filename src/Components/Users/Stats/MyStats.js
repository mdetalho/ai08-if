import * as React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { BlurView } from 'expo-blur';
import styles from '../../../styles/App.css'

const MyStats = ({ navigation }) => {

  const navigateToAwardPage = () => {
    navigation.navigate('Rewards');
  };

  const navigateToFeedPage = () => {
    navigation.navigate('IfFeed');
  };

    return( 
      <View style={{top: '10%'}}>
        <View style={styles.title_container}>
          <Text style={styles.purple_title}> Mes stats </Text>
        </View>
        <BlurView intensity={80} tint="light" style={styles.my_stats_container}>
          <Text style={{fontSize:17, fontWeight: '600'}}> 1234 points gagnés </Text>
          <View style={{flexDirection: 'row', top: '2%', bottom: '2%'}}>
            <Text style={styles.body} > 12 défis ratés </Text>
            <Text style={styles.body} > 24 défis validés </Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity style={styles.white_button} onPress= {navigateToAwardPage} >
              <Text style={styles.title_button}>Mes récompenses</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.white_button} onPress= {navigateToFeedPage} >
              <Text style={styles.title_button}>IF Feed</Text>
            </TouchableOpacity>
            </View>
        </BlurView>
      </View>
    )
};

export default MyStats;
