# ai08-IF

## Set up 
- Installer expo sur mobile
- Installer expo sur ordi `npm install --global expo-cli`
- Installer Node v14.18.1 et nvm (Node Version Manager)
- `nvm use  v14.18.1`
- run : `yarn` ou `yarn install`

## Good Tips Dev 
- Changer `initialRouteName` in App.js en mettant le nom de sa vue
- Lorsqu'un fichier est enregistré, expo se met à jour. Si jamais le changement ne se voit pas, secouer le tel pour avoir accès au menu de dev et appuyer sur Reload. Le build recharge. 

## Architecture 
- App.js avec le router initial 
- src/Pages : ce sont les vues = une page pour l'utilisateur 
- src/Composant : des éléments d'une vue (logo, bloc...)

## Run the project
- `expo start`
