import * as React from 'react';
import { Image } from 'react-native';

const IFLogo = () => {
    return(
        <Image
            source={require('../../../assets/if_logo.png')}
            style={{
                flex: 1,
                position: 'relative',
                top: '5%',
                left: '10%',
                width: '80%',
                height: '30%',
                resizeMode: 'contain'}}
            >
        </Image>
    )
}

export default IFLogo