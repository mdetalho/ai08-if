import { View, Text, TouchableOpacity, Vibration, Image, Dimensions } from "react-native";
import { BlurView } from "expo-blur";
import styles from "../styles/Admin.css";
import BackIcon from "../Components/BackIcon";
import AdminButton from "../Components/AdminButton";
import Profile from "./Profile";
import ListSubmissionCards from "../Components/Users/Defis/ListSubmissionCards";
import React, { useState, useEffect } from "react";

//back
import { Ionicons } from "@expo/vector-icons";
import { useSafeAreaInsets } from "react-native-safe-area-context";

const Admin = ({ navigation }) => {
  const [active_card, setActiveCard] = useState(0);


  if (typeof navigation.active_card === "undefined") {
    navigation.active_card = 123;
  }

  // Data structures, represent future models
  const submissions = [
    {
      challenger: "Marcel",
      key: 1,
      challenge_id: "1",
      profile_picture: require("../../assets/profile.jpg"),
      image: require("../../assets/admin/oiseau.jpg"),
    },
    {
      challenger: "Didier",
      key: 2,
      challenge_id: "1",
      profile_picture: require("../../assets/profile.jpg"),
      image: require("../../assets/admin/foot.png"),
    },
    {
      challenger: "Paul",
      key: 3,
      challenge_id: "1",
      profile_picture: require("../../assets/profile.jpg"),
      image: require("../../assets/admin/oiseau.jpg"),
    },
    {
      challenger: "Michle",
      key: 4,
      challenge_id: "1",
      profile_picture: require("../../assets/profile.jpg"),
      image: require("../../assets/admin/foot.png"),
    },
    {
      challenger: "Antonin",
      key: 5,
      challenge_id: "1",
      profile_picture: require("../../assets/profile.jpg"),
      image: require("../../assets/admin/oiseau.jpg"),
    },
    {
      challenger: "Michle",
      key: 6,
      challenge_id: "1",
      profile_picture: require("../../assets/profile.jpg"),
      image: require("../../assets/admin/foot.png"),
    },
    {
      challenger: "Tony",
      key: 7,
      challenge_id: "1",
      profile_picture: require("../../assets/profile.jpg"),
      image: require("../../assets/admin/oiseau.jpg"),
    },
    {
      challenger: "Arthur",
      key: 8,
      challenge_id: "1",
      profile_picture: require("../../assets/profile.jpg"),
      image: require("../../assets/admin/foot.png"),
    },
  ];

  const challenges = [
    {
      id: 1,
      name: "Challenge 1",
      description: "Description 1",
      // picture: require("../../assets/admin/challenges/Challenge1.jpg"),
    },
    {
      id: 2,
      name: "Challenge 2",
      description: "Description 2",
      // picture: require("../../assets/admin/challenges/Challenge2.jpg"),
    },
  ];

  previousCard = () => {
    if (active_card > 0) {
      setActiveCard(active_card - 1);
    }
    
  };

  nextCard = () => {
    if (active_card < submissions.length) { // not submission.length - 1 because we don't want to go to the last card
      setActiveCard(active_card + 1);
    }
  };

  // functions interfaces with the backend
  function report() {
    alert("submission reported");
    nextCard();
  }

  function revert() {
    previousCard();
  } 

  const approve = () => {
    nextCard();
  };

  const refuse = () => {
    nextCard();
  };


  const stop = require('../../assets/stop.png')
  const valid = require('../../assets/valid.png')
  const caution = require('../../assets/caution.png')
  const back = require('../../assets/back.png')
  const imageWidth = Dimensions.get('window').width / 14
  const bigImageWidth = Dimensions.get('window').width / 8
  

  return (
    <View style={styles.flex_content_box}>
      <View style={{display: 'flex', flexDirection: 'row', alignItems: 'flex-start'}}>
        <View style= {{alignItems:'center', backgroundColor: 'rgba(0, 18, 180, 1.0)', borderRadius: 60, padding: '2%', margin: '2%'}}>
        <TouchableOpacity
            style={{
              zIndex: 1,
            }}
            onPress={() => navigation.replace('Main')}
          >
            <Ionicons name="arrow-back" size={28} color="#999999" />
        </TouchableOpacity>
      </View>
      <View style= {{alignItems:'center', backgroundColor: 'rgba(0, 18, 180, 1.0)', borderRadius: 15, padding: '2%', marginRight: '2%', marginTop: '2%'}}>
          <Text style={{
          fontSize: 25, 
          color: 'rgba(255, 255, 255, 1.0)',
          fontWeight: 'bold'
          }}> Interface administrateur </Text>
      </View>

      </View>
      
      <ListSubmissionCards
        submissions={submissions}
        active_card={active_card}
      />


      <View style={{flexDirection:'row', justifyContent:'center'}} >
          <TouchableOpacity onPress={() => revert()}>
            <Image
              source={back}
              resizeMode='cover'
              style={{
                  width: bigImageWidth,
                  height: bigImageWidth,
                  marginLeft:'2%',
                  marginTop: '5%'
              }}
            />
          </TouchableOpacity>
          <TouchableOpacity style={styles.red_button} onPress={() => refuse()}>
            <Image
              source={stop}
              resizeMode='cover'
              style={{
                  width: imageWidth,
                  height: imageWidth,
                  marginLeft:'2%',
                  marginTop: '5%'
              }}
            />
            <Text style={styles.title_purple_button}>Refuser</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.green_button} onPress={() => approve()}>
          <Image
              source={valid}
              resizeMode='cover'
              style={{
                  width: imageWidth,
                  height: imageWidth,
                  marginLeft:'2%',
                  marginTop: '5%'
              }}
            />
            <Text style={styles.title_purple_button}>Valider</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => report()}>
            <Image
              source={caution}
              resizeMode='cover'
              style={{
                  width: bigImageWidth,
                  height: bigImageWidth,
                  marginLeft:'2%',
                  marginTop: '5%'
              }}
            />
          </TouchableOpacity>
      </View>
    </View>
  );
};

export default Admin;
