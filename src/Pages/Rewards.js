import * as React from 'react';
import { View, Text, Dimensions, Image, ImageBackground} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import ScrollGradient from '../Components/Gradients/ScrollGradient';
import MyRewards from '../Components/Users/Reward/MyRewards';
import styles from '../styles/App.css';


const Rewards = () => {

  const if_background = require('../../assets/if_background.jpg')
  const award = require('../../assets/award.png')
  const imageWidth = Dimensions.get('window').width / 10

  return(
    <ImageBackground
      source={if_background}
      style={{width: '100%', height: '100%'}}
      resizeMode= 'cover'
    >
      <ScrollGradient/>
        <ScrollView style={{flex: 1}} contentContainerStyle={{ flexGrow: 1}} contentContainerStyle={{alignItems: 'center'}}>

            <View style={{flex:1, top: 50, width: '90%'}}>         
                <View style= {styles.pages_title_container}>
                    <Image 
                    source={award}
                    resizeMode='cover'
                    style={{
                        width: imageWidth,
                        height: imageWidth,
                        margin:'2%'
                    }}
                    />
                    <Text style={{
                    fontSize: 25, 
                    color: 'rgba(0, 18, 180, 1.0)',
                    fontWeight: 'bold'
                    }}> Mes récompenses </Text>
                </View>
            </View>
            <View style={{marginTop: 70, width: '90%'}}>
                <View style= {{alignItems:'center', justifyContent:'center'}}></View>
                <MyRewards/>
                <MyRewards/>
                <MyRewards/>
                <MyRewards/>
                <MyRewards/>
                <MyRewards/>
                <MyRewards/>
                <MyRewards/>
                <MyRewards/>
                <View style={{height:200}}></View>
            </View>
        </ScrollView>
    </ImageBackground>
    )
}

export default Rewards
