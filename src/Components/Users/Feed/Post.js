import * as React from 'react';
import { View, Text, Image, Dimensions } from 'react-native';
import { BlurView } from 'expo-blur';
import styles from '../../../styles/App.css'

const Post = () => {

  const post = require('../../../../assets/post.jpg')
  const profile = require('../../../../assets/profile.jpg')
  const scale = Dimensions.get('window').width

  return( 
    <BlurView intensity={80} tint="light" style={styles.my_post_container}>
      <Image
        style={{
          alignSelf: 'center',
          height: scale,
          width: scale
        }}
        source={post}
        resizeMode="cover"
      />
      <View style={{display:'flex', flexDirection:'row', justifyContent:'flex-start'}}>
        <Image 
          source={profile}
          resizeMode='cover'
          style={{
            width: 50,
            height: 50,
            borderRadius: 40
          }}
        />
        <Text style={{flex:1, padding:'2%'}}> Bidule a participé au défi Selfie avec Joris</Text>
        </View>
    </BlurView>
  )
}

export default Post