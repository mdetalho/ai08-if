import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  // WELCOME FLOW
  button: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.7)',
    borderRadius: 20,
    padding: '5%',
    margin:'2%'
  },

  pages_title_container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems:'center', 
    backgroundColor: 'rgba(255, 255, 255, 1.0)',
    borderRadius: 15,
    padding: '2%'
  },

  blur_wrapper: {
    flex: 3,
    display: 'flex',
  },

  welcome: {
    fontSize: 20,
    position: "relative", 
    padding: '2%',
    margin:'5%',
    textAlign:'justify'
  },

  welcome_title_button: {
    fontSize: 20,
    fontWeight: 'bold',
  },

  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    width: '80%',
    left: '10%',
    borderRadius: 15, 
    overflow: 'hidden',
    paddingBottom: '10%',
    paddingTop: '10%',
  },

  input: {
    color: '#5D1CB0',
    fontSize: 18,
    position: "relative",
    margin: '7%',
    borderBottomColor: '#5D1CB0',
    borderBottomWidth: 2,
    width: '80%'
  },

  confidentiality_title: {
    fontSize: 20, 
    fontWeight: 'bold', 
    width: '60%', 
    textAlign: 'center'
  },
  
  permissions: {
    fontSize: 18, 
    textAlign: 'center', 
    margin: '8%', 
    textAlign: 'center'
  },

  savoir_plus: {
    fontSize: 16, 
    fontWeight: '500', 
    color: 'black',
    padding:'2%'
  },

  // Profile styles
  my_stats_container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    borderBottomStartRadius: 15, 
    borderBottomEndRadius: 15, 
    overflow: 'hidden',
    paddingTop: '5%',
    },

  my_stats_button: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    backgroundColor: 'rgba(255, 255, 255, 0.7)',
    color: 'black',
    borderRadius: 15,
    margin: "10%",
    padding: '3%',
  },

  inner_button: {
    fontSize: 14,
    fontWeight: '500', 
    textAlignVertical: 'center',
    textAlign: 'center'
  },

  my_settings_container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    borderRadius: 15, 
    overflow: 'hidden',
    },

  my_settings_button: {
    display: 'flex',
    zIndex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgba(255, 255, 255, 1.0)',
    color: 'black',
    borderRadius: 15,
    padding: '2%',
    margin:'2%'
    },
  
  // Defis styles
  my_defis_settings_container: {
    width : '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    borderRadius: 15, 
    overflow: 'hidden',
    padding : '4% 2%',
    },
  my_defis_settings_blur: {
    borderRadius: 25, 
    overflow: 'hidden',
    paddingVertical : '2%',
    paddingHorizontal : '3%',
    marginHorizontal : '1%',
    marginVertical:'1%'
  },
  my_defis_inner_button: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  my_defis_inner_button_grey: {
    fontSize: 25,
    fontWeight: 'bold',
    color : '#4C4C4C',
  },

//IF page
  my_post_container: {
    display: 'flex',
    borderRadius: 15, 
    overflow: 'hidden',
    marginTop: '5%',
    },
  
//Awards page
  my_rewards_container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    borderRadius: 15, 
    overflow: 'hidden',
    paddingTop: '3%',
    marginBottom:'5%'
    },

  modal_container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    width: '80%',
    left: '10%',
    borderRadius: 15, 
    overflow: 'hidden',
    paddingBottom: '5%',
    paddingTop: '5%',
    backgroundColor: '#FFFFFF'
  },

  purple_button: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    backgroundColor: 'rgba(0, 18, 180, 0.8)',
    borderRadius: 20,
    margin: "5%",
    padding: '2%',
  },

  face: {
    display: 'flex',
    alignItems:'center',
    backgroundColor: 'rgba(0, 18, 180, 0.8)',
    borderRadius: 20,
    margin: "10%",
    padding: '5%',
  },

  white_button: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    backgroundColor: 'rgba(255, 255, 255, 0.7)',
    borderRadius: 20,
    margin: "5%",
    padding: '2%',
  },

  title_purple_button: {
    fontSize: 16, 
    fontWeight: '500', 
    color: 'white',
    padding:'2%'
  },

  title_button: {
    fontSize: 16, 
    fontWeight: '500', 
    color: 'black',
    padding:'2%'
  },

  modal_button: {
    display: 'flex',
    backgroundColor: 'rgba(143, 53, 138, 1.0)',
    borderRadius: 20,
    padding: '3%',
  },

  title: {
    fontSize:20, 
    fontWeight: '600', 
    padding:'1%',
  },

  body: {
    fontSize:14,
    textAlign: 'justify',
    padding:'3%'
  },

  title_container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    width: '100%',
    borderTopStartRadius: 15,
    borderTopEndRadius: 15,
    overflow: 'hidden',
    paddingTop: '1%',
    backgroundColor: '#FFFFFF'
  },

  purple_title: {
    fontSize:20, 
    fontWeight: '600', 
    padding:'1%',
    color: 'rgba(0, 18, 180, 0.7)',
  },

//Top users page

other_user: {
  fontSize:12, 
  fontWeight: '600', 
  margin: 5, 
  paddingTop: '0%'
},

current_user : {
  fontSize:12, 
  fontWeight: '900', 
  margin: 5, 
  paddingTop: '10%'

}
})


