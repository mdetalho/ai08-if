import * as React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { BlurView } from 'expo-blur';
import styles from '../styles/App.css'
  
  const MySettings = ({navigation}) => {
    
    const goToAdmin = () => {
      navigation.replace('Admin');
    };

    return (
      <View style={{top: '20%'}}>
        <View style={styles.title_container}>
          <Text style={styles.purple_title}> Paramètres du compte </Text>
        </View>
        <BlurView intensity={80} tint="light" style={{...styles.my_stats_container, padding: 10}}>
          <TouchableOpacity style={styles.my_settings_button}>
            <Text style={styles.inner_button}>Masquer mes informations publiques</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.my_settings_button} onPress= {() => goToAdmin()} >
            <Text style={styles.inner_button}>Acceder à l'interface administrateur</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.my_settings_button} >
            <Text style={styles.inner_button}>Me deconnecter</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.my_settings_button} >
            <Text style={styles.inner_button}>Contacter le support</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.my_settings_button} >
            <Text style={styles.inner_button}>Supprimer mon compte</Text>
          </TouchableOpacity>
        </BlurView>
      </View> 
    );
  };
  export default MySettings;