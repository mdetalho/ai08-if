import React, {useState} from 'react';
import {
  Text, View, ImageBackground, Button
} from 'react-native';
import BRLinearGradient from '../Components/Gradients/BRLinearGradient';
import IFLogo from '../Components/IFLogo/IFLogo';
import { BlurView } from 'expo-blur';
import { TouchableOpacity } from 'react-native-gesture-handler';
import styles from '../styles/App.css'
import BackIcon from '../Components/BackIcon';
import { Camera } from 'expo-camera';

const PermissionsPage = ({ navigation }) => {
  const [cameraPermission, setCameraPermission] = useState(null);

  /**
   * Get required permissions statuses
   * @returns {Promise<void>}
   */
   const getCurrentPermissions = async () => {
    setCameraPermission(await Camera.getCameraPermissionsAsync());
  };

  const askCameraPermission = async () => {
    setCameraPermission(await Camera.requestCameraPermissionsAsync());
  };

  const goToNextPage = () => {
      navigation.replace('Main');
  };

  const goToPreviousPage = () => {
    navigation.goBack('Authentification');
  };


  const if_background = require('../../assets/if_background.jpg')

  const permissions = "Conditions spécifiques aux défis. Vous acceptez que les images que vous nous envoyez via cette application soient publiées, et acceptez qu’elles servent soient republiées par l’association imaginariuum festival."

return (
  <ImageBackground
    source={if_background}
    style={{width: '100%', height: '100%', zIndex:4}}
    resizeMode= 'cover'
  >
  <BackIcon onPress= {() => goToPreviousPage()}/>
  <BRLinearGradient/>
    <View style={{flex:1, justifyContent: 'space-evenly'}}>
      <IFLogo/>
      <View style={styles.blur_wrapper}>
        
        <BlurView intensity={80} tint="light" style={styles.container}>
        <Text style={styles.confidentiality_title}>Conditions de confidentialités</Text>
        <Text style={styles.permissions}>{permissions}</Text>
        <View>
          <TouchableOpacity style={styles.button} onPress= {askCameraPermission} >
            <Text style={styles.welcome_title_button}>Autoriser la caméra</Text>
          </TouchableOpacity>
        </View>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity style={styles.button} onPress= {goToNextPage} >
            <Text style={styles.welcome_title_button}>J'accepte</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress= {goToNextPage} >
            <Text style={styles.welcome_title_button}>Je refuse</Text>
          </TouchableOpacity>
        </View>
        <View style={{flexDirection:'row', marginTop:'5%'}}>
          <TouchableOpacity onPress= {goToNextPage}>
            <Text style={styles.savoir_plus}>En savoir plus</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.containerRow}>
      </View>
        </BlurView>
        
        
      </View>
    </View>   
  </ImageBackground>
  );
}

export default PermissionsPage;
